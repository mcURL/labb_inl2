package Labb.uppgift3;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        Predicate<String> vowelFilter = Pattern.compile("[aAeEiIoOuUyY][^ ]*[aAeEiIoOuUyY]")
                .asPredicate();

        List<String> wordList = List.of("Avancerad", "java", "inlämning", "två", "labb", "uppgift", "tre", "regex");

        List<String> filteredList = wordList.stream()
                .filter(vowelFilter)
                .toList();

        filteredList.forEach(System.out::println);
        System.out.println(filteredList);
    }
}
