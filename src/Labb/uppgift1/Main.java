package Labb.uppgift1;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Person> personList = List.of(
                new Person("Martina", "female", 36000),
                new Person("Björn", "male", 42000),
                new Person("Mario", "male", 51000),
                new Person("Mara", "female", 37000),
                new Person("Ina", "female", 39000),
                new Person("Tina", "female", 45000),
                new Person("Johan", "male", 34000),
                new Person("Marta", "female", 29000),
                new Person("Martin", "male", 34000),
                new Person("Na", "female", 36000)
        );

        int femAvg = (int) personList.stream()
                .filter(p -> p.getGender().equals("female"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();
        System.out.println("female average salary: " + femAvg);


        Map<String, Double> averageMaleAndFemale = personList.stream()
                .collect(Collectors.groupingBy(Person::getGender, Collectors.averagingDouble((Person::getSalary))));
        System.out.println("Average salary: " + averageMaleAndFemale);

        Person maxSalary = personList.stream()
                .max(Comparator.comparing(Person::getSalary))
                .orElseThrow();
        System.out.println("maxSalary -> " + maxSalary);


        System.out.println("minSalary -> " + personList.stream()
                .min(Comparator.comparing(Person::getSalary)).orElseThrow());
    }}
