package Labb.uppgift4;

import java.util.stream.IntStream;

class Primal implements Runnable {
    int start;
    int end;

    public Primal(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public synchronized void run() {
        long count = IntStream.rangeClosed(start, end)
                .filter(Primal::isPrime)
                .count();
        System.out.println("There are " + count + " primal numbers in the range from " + start + " to " + end);
    }

    static boolean isPrime(int n) {
        if (n == 0 || n == 1)
            return false;

        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {

        long startTime = System.nanoTime();
        Thread thread = new Thread(new Primal(1, 150000));
        Thread thread2 = new Thread(new Primal(150001, 300000));
        Thread thread3 = new Thread(new Primal(300001, 500000));
        thread.start();
        thread2.start();
        thread3.start();
        thread.join();
        thread2.join();
        thread3.join();
        long endTime = System.nanoTime();
        double time = (endTime - startTime) / 1000000000.0;
        System.out.println("Tiden det tog med tre trådar: " + time);
        System.out.println("-------------------------------------");

        long startTime2 = System.nanoTime();
        new Primal(1, 500000).run();
        long endTime2 = System.nanoTime();
        double time2 = (endTime2 - startTime2) / 1000000000.0;
        System.out.println("Tiden det tog med en: " + time2);


    }
}
