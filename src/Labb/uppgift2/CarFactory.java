package Labb.uppgift2;

public class CarFactory {

    public Car createCar(CarType carType) {
        return switch (carType) {
            case ELECTRIC -> new Tesla();
            case SPORT -> new Porsche();
            //case HYBRID -> new Toyota();
            case HYBRID -> new Lexus();
        };
    }

}
