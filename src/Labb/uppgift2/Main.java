package Labb.uppgift2;

public class Main {
    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory();

        Car sportCar = carFactory.createCar(CarType.SPORT);
        Car electricCar = carFactory.createCar(CarType.ELECTRIC);
        Car hybridCar = carFactory.createCar(CarType.HYBRID);

        sportCar.makeCar();
        electricCar.makeCar();
        hybridCar.makeCar();

    }
}
